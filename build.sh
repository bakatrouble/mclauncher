#!/bin/zsh
pyclean () {
        find . -type f -name "*.py[co]" -delete
        find . -type d -name "__pycache__" -delete
}

pyclean
source .env/bin/activate
pyinstaller -F -w -y --hiddenimport classes --hiddenimport classes.pages --hiddenimport functions --hiddenimport ui --hiddenimport views main.py
deactivate

pyclean
wine cmd /c 'C:\Python34\python -m PyInstaller -F -w -y --hiddenimport classes --hiddenimport classes.pages --hiddenimport functions --hiddenimport ui --hiddenimport views main.py'
