from PySide.QtGui import QGridLayout
from classes import StackedWidget
from classes.pages import *


class MainLayout(QGridLayout):
    def __init__(self, win, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.win = win

        self.setContentsMargins(0, 0, 0, 0)

        win.login_page = LoginPage(self.win)
        win.register_page = RegisterPage(self.win)
        win.restore_page = RestorePage(self.win)
        win.play_page = PlayPage(self.win)

        self.stack = StackedWidget()
        self.stack.addWidget(win.login_page)
        self.stack.addWidget(win.register_page)
        self.stack.addWidget(win.restore_page)
        self.stack.addWidget(win.play_page)

        self.addWidget(self.stack)
