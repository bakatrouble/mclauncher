from PySide.QtGui import QMainWindow, QWidget

from classes.main_layout import MainLayout
from ui.modal import Modal
from ui import resources


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setObjectName("MainWindow")
        self.setFixedSize(300, 400)
        self.setWindowTitle("CirnoCraft")
        self.mainLayout = MainLayout(self)
        self.stack = self.mainLayout.stack
        self.widget = QWidget()
        self.widget.setLayout(self.mainLayout)

        # self.setStyleSheet("background-color: #fff;")

        self.modal = Modal(self.widget)
        self.setCentralWidget(self.widget)
