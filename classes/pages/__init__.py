from classes.pages.login_page import LoginPage
from classes.pages.register_page import RegisterPage
from classes.pages.restore_page import RestorePage
from classes.pages.play_page import PlayPage
