import time
from PySide.QtCore import QThread, Signal, Qt
from PySide.QtGui import QWidget
from views.ui_login import Ui_LoginPage
from functions.auth import login
import config


class DoLogin(QThread):
    username = ""
    password = ""
    done = Signal(list)

    def run(self):
        try:
            res = login(self.username, self.password)
        except ValueError:
            self.done.emit([False, "ValueError"])
        except ConnectionError:
            self.done.emit([False, "ConnectionError"])
        else:
            self.done.emit([True, res])


class LoginPage(Ui_LoginPage, QWidget):
    do_login_thread = None

    def __init__(self, win):
        """:type win: classes.MainWindow"""
        super(LoginPage, self).__init__()
        self.win = win
        self.setupUi(self)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.assign_widgets()

    def check_saved(self):
        if config.saved_config.user["remembered"]:
            self.username_input.setText(config.saved_config.user["username"])
            self.password_input.setText(config.saved_config.user["password"])
            self.do_login()

    def assign_widgets(self):
        self.to_register.clicked.connect(lambda: self.win.stack.setCurrentIndex(1))
        self.to_restore.clicked.connect(lambda: self.win.stack.setCurrentIndex(2))
        self.do_login_btn.clicked.connect(self.do_login)

    def keyPressEvent(self, evt):
        if not evt.isAutoRepeat() and \
                evt.key() in [Qt.Key_Enter, Qt.Key_Return] and \
                not self.win.modal.isVisible():
            self.do_login()
        evt.accept()

    def result(self, val):
        self.win.modal.close_modal()
        if val[0]:
            config.memory_config["login_response"] = val[1]["response"]
            config.memory_config["user"] = {
                "username": self.do_login_thread.username,
                "password": self.do_login_thread.password
            }
            if self.remember_me.isChecked():
                config.saved_config.user = {
                    "remembered": True,
                    "username": self.do_login_thread.username,
                    "password": self.do_login_thread.password
                }
            self.to_play_page()
        else:
            if val[1] == "ValueError":
                self.win.modal.message_box("Login or password you have entered are incorrect. Please, check them.")
            elif val[1] == "ConnectionError":
                self.win.modal.message_box("Please, check your internet connection and try later.")

    def to_play_page(self):
        self.win.play_page.set_editions_list([x["display_name"] for x in
                                              config.memory_config["login_response"]["editions"]])
        self.win.play_page.init_page()
        self.win.stack.setCurrentIndex(3)

    def do_login(self):
        self.do_login_thread = DoLogin()
        self.win.modal.progress_bar("Verifying...")
        self.do_login_thread.done.connect(self.result, Qt.QueuedConnection)
        self.do_login_thread.username = self.username_input.text()
        self.do_login_thread.password = self.password_input.text()
        self.do_login_thread.start()
