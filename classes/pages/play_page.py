import webbrowser

from PySide.QtCore import QThread, Signal, Qt

from functions.downloads import dl_file, un_tar
from os import path
import os
import json
import hashlib
from requests import RequestException
from shutil import rmtree as rmdir
from PySide.QtGui import QWidget
from views.ui_play import Ui_PlayPage
from config import memory_config, saved_config
from functions.run import run_game, JavaNotFound


class PlayPage(Ui_PlayPage, QWidget):
    ram_values = [1024, 2048]
    ram_names = ["1 GB", "2 GB"]
    current_edition = args = None
    dl_thread = rg_thread = prepare_thread = None

    def __init__(self, win):
        """:type win: classes.MainWindow"""
        super(PlayPage, self).__init__()
        self.win = win
        self.setupUi(self)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.assign_widgets()

    def init_page(self):
        self.current_edition = memory_config["login_response"]["editions"][self.editions_list.currentIndex()]
        if self.check_version() and self.check_edition():
            self.show_play_btns()
        else:
            self.hide_play_btns()

    def show_play_btns(self):
        self.play_layout.setVisible(True)
        self.dl_layout.setVisible(False)

    def hide_play_btns(self):
        self.play_layout.setVisible(False)
        self.dl_layout.setVisible(True)

    def save_ram_setting(self, val):
        saved_config.ram_setting = self.ram_values[val]

    def clear_data(self, versions=True):
        if versions:
            saved_config.downloaded_versions = []
        else:
            saved_config.downloaded_editions = []

        if self.check_version() and self.check_edition():
            self.show_play_btns()
        else:
            self.hide_play_btns()

        self.win.modal.message_box("Data was cleared")

    def assign_widgets(self):
        self.ram_list.insertItems(0, self.ram_names)
        self.ram_list.setCurrentIndex(self.ram_values.index(saved_config.ram_setting))
        self.ram_list.currentIndexChanged.connect(self.save_ram_setting)

        self.editions_list.currentIndexChanged.connect(self.init_page)

        self.online_btn.clicked.connect(self.play)
        self.offline_btn.clicked.connect(self.play_offline)
        self.dl_btn.clicked.connect(lambda: (self.check_edition(True) if self.check_version(True) else None))

        self.clear_versions.clicked.connect(lambda: self.clear_data())
        self.clear_mods.clicked.connect(lambda: self.clear_data(False))

    def set_editions_list(self, lst):
        self.editions_list.clear()
        self.editions_list.insertItems(0, lst)

    def check_version(self, dl=False, ask=True):
        if self.current_edition["game_version"] not in saved_config.downloaded_versions:
            if dl:
                url = next(x for x in
                           memory_config["login_response"]["versions"]
                           if x["version"] == self.current_edition["game_version"])["gzip"]
                destination = path.join("game", self.current_edition["game_version"])

                def cb_before():
                    if not path.exists(destination):
                        os.makedirs(destination)
                    else:
                        rmdir(destination)
                        os.makedirs(destination)

                def cb_after():
                    self.win.modal.close_modal()
                    saved_config.downloaded_versions = saved_config.downloaded_versions + \
                                                       [self.current_edition["game_version"]]
                    if self.check_edition(True, False):
                        self.show_play_btns()

                if ask:
                    self.win.modal.yes_no_dialog(
                        "Do you want to download the game data now?",
                        lambda: self.download_and_un_tar(url, destination, cb_before, cb_after, True, True)
                    )
                else:
                    self.download_and_un_tar(url, destination, cb_before, cb_after, True, True)
            return False
        return True

    def check_edition(self, dl=False, ask=True):
        if self.current_edition["id"] not in saved_config.downloaded_editions and self.current_edition["mods_active"]:
            if dl:
                url = self.current_edition["mods_gzip"]
                destination = path.join("gamedata", self.current_edition["id"])
                if not path.exists(destination):
                    os.makedirs(destination)

                def cb_before():
                    pass

                def cb_after():
                    self.win.modal.close_modal()
                    saved_config.downloaded_editions = \
                        saved_config.downloaded_editions + [self.current_edition["id"]]
                    self.show_play_btns()

                if ask:
                    self.win.modal.yes_no_dialog(
                        "Do you want to download the mods archive now?",
                        lambda: self.download_and_un_tar(url, destination, cb_before, cb_after, False, False)
                    )
                else:
                    self.download_and_un_tar(url, destination, cb_before, cb_after, False, False)
            return False
        return True

    def play_offline(self):
        self.play(True)

    def play(self, offline=False):
        self.current_edition = memory_config["login_response"]["editions"][self.editions_list.currentIndex()]
        if self.check_version(True):
            game_dir = path.join("gamedata", self.current_edition["id"])
            if not path.exists(path.join(game_dir, "config")):
                os.makedirs(path.join(game_dir, "config"))
            self.args = list(json.loads(self.current_edition["args"]))
            self.args += ["--username", memory_config["user"]["username"],
                          "--password", memory_config["user"]["password"]]
            if not offline:
                self.args += ["--server", self.current_edition["server_address"],
                              "--port", str(self.current_edition["server_port"])]
            if self.current_edition["mods_active"]:
                self.win.modal.progress_bar("Preparing...")
                self.prepare_thread = self.PrepareThread(
                    game_dir,
                    path.basename(self.current_edition["mods_gzip"]),
                    self.current_edition["sha1"]
                )
                self.prepare_thread.success.connect(self.prepared)
                self.prepare_thread.error.connect(self.prepare_error)
            else:
                self.prepared()

    def prepare_error(self, code):
        if code == 1:
            self.win.modal.message_box("Mods package has wrong hash, please redownload it")

    def prepared(self):
        self.win.modal.close_modal()
        self.win.modal.progress_bar("Game is running...", True)
        self.rg_thread = self.RunGameThread(
            self.current_edition["game_version"],
            self.current_edition["id"],
            self.current_edition["main_class"],
            self.args
        )
        self.rg_thread.game_closed.connect(self.win.modal.close_modal)
        self.rg_thread.error.connect(lambda s: self.win.modal.yes_no_dialog(s, lambda: webbrowser.open("https://www.java.com/inc/BrowserRedirect1.jsp?locale=ru")))
        self.rg_thread.start()

    class PrepareThread(QThread):
        success = Signal()
        error = Signal(int)

        def __init__(self, destination, filename, sha1, parent=None):
            super().__init__(parent)
            self.destination = destination
            self.filename = filename
            self.sha1 = sha1
            self.start()

        def run(self):
            if path.exists(path.join(self.destination, "mods")):
                rmdir(path.join(self.destination, "mods"))
            # TODO: список для очистки

            file_hash = hashlib.sha1()
            f = open(path.join(self.destination, self.filename), "rb")
            while True:
                piece = f.read(64 * 1024)
                if not piece:
                    break
                file_hash.update(piece)
            if file_hash.hexdigest() != self.sha1:
                self.error.emit(1)
                return

            un_tar(
                path.join(self.destination, self.filename),
                self.destination,
                False,
                self.success.emit
            )

    class RunGameThread(QThread):
        error = Signal(str)
        game_closed = Signal()

        def __init__(self, version, edition, main_class, args, parent=None):
            super().__init__(parent)
            self.version = version
            self.edition = edition
            self.main_class = main_class
            self.args = args

        def run(self):
            try:
                proc = run_game(
                    path.join("game", self.version),
                    path.join("gamedata", self.edition),
                    self.main_class,
                    self.version,
                    xmx=saved_config.ram_setting,
                    more=self.args
                )
                proc.wait()
                self.game_closed.emit()
            except JavaNotFound as e:
                self.error.emit(e.message)

    def download_and_un_tar(self, url, destination, cb_before, cb_after, untar, unlink):
        self.win.modal.progress_bar("Preparing...")

        try:
            cb_before()
        except PermissionError:
            self.win.modal.message_box("Couldn't write to directory. Please either run the launcher as administrator, "
                                       "reinstall the launcher or set directory write permissions manually.")

        self.dl_thread = self.DownloadThread(
            url,
            destination,
            lambda x: self.win.modal.label.setText("Downloading 0%\n0/{:.2f} MBytes".format(x)),
            lambda x, y: self.win.modal.label.setText("Downloading {:.0%}\n{:.2f}/{:.2f} MBytes".format(x / y, x, y)),
            untar,
            lambda: self.win.modal.label.setText("Unpacking..."),
            cb_after,
            unlink,
            self.win.modal.message_box
        )

    class DownloadThread(QThread):
        got_length = Signal(float)
        got_chunk = Signal(float, float)
        un_tar_start = Signal()
        all_finished = Signal()
        onerror = Signal(str)

        def __init__(self, url, destination, start_cb, step_cb, do_un_tar, un_tar_cb, finish_cb,
                     unlink, error_cb, parent=None):
            super().__init__(parent)
            self.url = url
            self.destination = destination
            self.do_un_tar = do_un_tar
            self.unlink = unlink
            self.got_length.connect(start_cb)
            self.got_chunk.connect(step_cb)
            self.un_tar_start.connect(un_tar_cb)
            self.all_finished.connect(finish_cb)
            self.onerror.connect(error_cb)
            self.start()

        def dl_finished(self):
            if self.do_un_tar:
                self.un_tar_start.emit()
                self.start_un_tar()
            else:
                self.all_finished.emit()

        def run(self):
            try:
                dl_file(
                    self.url,
                    self.destination,
                    self.got_length.emit,
                    self.got_chunk.emit,
                    self.dl_finished
                )
            except RequestException:
                self.onerror.emit("An error occurred while downloading. Please, check your internet connection "
                                  "and try later.")

        def start_un_tar(self):
            try:
                un_tar(
                    path.join(self.destination, path.basename(self.url)),
                    self.destination,
                    self.unlink,
                    self.all_finished.emit
                )
            except PermissionError:
                self.onerror.emit("Couldn't unpack the archibe to directory. Please either run the "
                                  "launcher as administrator, reinstall the launcher or set directory "
                                  "write permissions manually.")
