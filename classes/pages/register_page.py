from PySide.QtCore import QThread, Signal, Qt
from PySide.QtGui import QWidget

from functions.auth import register
from views.ui_register import Ui_RegisterPage


class DoRegister(QThread):
    username = password = password2 = email = ""
    done = Signal(list)

    def run(self):
        try:
            res = register(self.username, self.password, self.password2, self.email)
        except ConnectionError:
            self.done.emit([False, "ConnectionError"])
        except Exception as e:
            self.done.emit([False, e.args[0]])
        else:
            self.done.emit([True, res])


class RegisterPage(Ui_RegisterPage, QWidget):
    do_register_thread = None

    def __init__(self, win):
        """:type win: classes.MainWindow"""
        super(RegisterPage, self).__init__()
        self.win = win
        self.setupUi(self)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.assign_widgets()

    def assign_widgets(self):
        self.to_login.clicked.connect(lambda: self.win.stack.setCurrentIndex(0))
        self.do_register_btn.clicked.connect(self.do_register)

    def keyPressEvent(self, evt):
        if not evt.isAutoRepeat() and \
                evt.key() in [Qt.Key_Enter, Qt.Key_Return] and \
                not self.win.modal.isVisible():
            self.do_register()
        evt.accept()

    def do_register(self):
        self.win.modal.progress_bar("Sending data...")
        self.do_register_thread = DoRegister()
        self.do_register_thread.username = self.username_input.text()
        self.do_register_thread.password = self.password_input.text()
        self.do_register_thread.password2 = self.password_input2.text()
        self.do_register_thread.email = self.email_input.text()
        self.do_register_thread.done.connect(self.result)
        self.do_register_thread.start()

    def result(self, res):
        self.win.modal.close_modal()
        if not res[0]:
            if res[1] == "ConnectionError":
                self.win.modal.message_box("Please, check your internet connection and try later.")
            else:
                self.win.modal.message_box(res[1])
        else:
            self.win.modal.message_box("You have successfully registered.")
            self.win.stack.setCurrentIndex(0)
