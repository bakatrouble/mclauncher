from PySide.QtCore import Qt
from PySide.QtGui import QWidget
from views.ui_restore import Ui_RestorePage


class RestorePage(Ui_RestorePage, QWidget):
    def __init__(self, win):
        """:type win: classes.MainWindow"""
        super(RestorePage, self).__init__()
        self.win = win
        self.setupUi(self)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.assign_widgets()

    def assign_widgets(self):
        self.to_login.clicked.connect(lambda: self.win.stack.setCurrentIndex(0))
