from PySide.QtCore import QTimeLine, Qt
from PySide.QtGui import QWidget, QPixmap, QPainter


class SlideWidget(QWidget):
    backwards = False

    def __init__(self, old_pixmap, new_pixmap, new_widget, backwards=False):
        QWidget.__init__(self, new_widget)

        self.backwards = backwards

        self.old_pixmap = old_pixmap
        self.new_pixmap = new_pixmap

        self.new_widget = new_widget
        self.pixmap_offset = 1.0

        self.timeline = QTimeLine()
        self.timeline.valueChanged.connect(self.animate)
        self.timeline.finished.connect(self.close)
        self.timeline.setDuration(200)
        self.timeline.setUpdateInterval(1000//60)
        self.timeline.setCurveShape(QTimeLine.EaseOutCurve)
        self.timeline.start()

        self.resize(new_widget.size())
        self.show()

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        try:
            if not self.backwards:
                painter.drawPixmap(-self.pixmap_offset*self.width(), 0, self.new_pixmap)
                painter.drawPixmap((1-self.pixmap_offset)*self.width(), 0, self.old_pixmap)
            else:
                painter.drawPixmap(-(1-self.pixmap_offset)*self.width(), 0, self.old_pixmap)
                painter.drawPixmap(self.pixmap_offset*self.width(), 0, self.new_pixmap)
        except AttributeError:
            pass
        painter.end()

    def animate(self, value):
        self.pixmap_offset = 1.0 - value
        self.repaint()

    def close(self, *args, **kwargs):
        super().close(*args, **kwargs)
        self.new_widget.show()


