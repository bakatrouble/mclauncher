from PySide.QtCore import Qt
from PySide.QtGui import QStackedWidget, QPixmap, QImage

from classes import SlideWidget


class StackedWidget(QStackedWidget):
    fader_widget = None

    def __init__(self, parent=None):
        QStackedWidget.__init__(self, parent)

    def setCurrentIndex(self, index):
        backwards = not self.currentIndex() >= index

        old_widget = self.currentWidget()
        new_widget = self.widget(index)

        old_pixmap = QPixmap.fromImage(QImage(":/images/background.png"))
        new_pixmap = QPixmap.fromImage(QImage(":/images/background.png"))
        old_widget.render(old_pixmap)
        new_widget.render(new_pixmap)

        new_widget.setVisible(False)
        old_widget.setVisible(False)

        self.fader_widget = SlideWidget(old_pixmap, new_pixmap, new_widget, backwards)
        QStackedWidget.setCurrentIndex(self, index)
