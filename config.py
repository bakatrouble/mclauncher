import pickle
from uuid import uuid4

from os import path

BIN_CONFIG = "config.bin"

BASE_URL = "http://wip.cirnocraft.ru/"
SERVERS_URL = BASE_URL + "api/servers/"

AUTH_URL = BASE_URL + "api/auth/"
LOGIN_URL = AUTH_URL + "authenticate/"
REGISTER_URL = AUTH_URL + "register/"
RESTORE_URL = AUTH_URL + "restore"

TIMEOUT = 2


class SavedConfig:
    _data = {
        "downloaded_versions": [],
        "downloaded_editions": [],
        "ram_setting": 1024,
        "client_id": uuid4().hex,
        "user": {
            "remembered": False,
            "username": "",
            "password": "",
        },
    }

    def __init__(self):
        if path.isfile(BIN_CONFIG):
            f = open(BIN_CONFIG, "rb")
            self.__dict__["_data"] = pickle.load(f)
            f.close()

            try:
                self.user
            except AttributeError:
                self.user = {
                    "remembered": False,
                    "username": "",
                    "password": "",
                }
        else:
            f = open(BIN_CONFIG, "wb")
            pickle.dump(self._data, f)
            f.close()

    def __getattr__(self, item):
        if item in self._data:
            return self._data[item]
        else:
            raise AttributeError()

    def __setattr__(self, key, value):
        self._data[key] = value
        f = open(BIN_CONFIG, "wb")
        pickle.dump(self._data, f)
        f.close()

saved_config = SavedConfig()

memory_config = {}
