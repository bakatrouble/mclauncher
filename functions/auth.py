import requests_wrap
from requests.exceptions import HTTPError, ConnectionError as RequestError, Timeout
from config import *
from hashlib import sha1


def login(username, password):
    """
    :rtype: dict
    :type username: str
    :type password: str
    """

    try:
        res = requests_wrap.post(LOGIN_URL, json={
            "username": username,
            "password": password,
            "clientToken": saved_config.client_id
        })
    except (HTTPError, RequestError, Timeout):
        raise ConnectionError
    if "error" in res.json():
        raise ValueError
    else:
        return {"response": servers_list()}


def servers_list():
    try:
        res = requests_wrap.get(SERVERS_URL)
    except (HTTPError, RequestError, Timeout):
        raise ConnectionError
    return res.json()


def register(username, password, password2, email):
    """
    :rtype: dict
    :type username: str
    :type password: str
    :type password2: str
    :type email: str
    """
    try:
        res = requests_wrap.post(REGISTER_URL, json={"username": username,
                                                     "password1": password, "password2": password2,
                                                     "email": email})
    except (HTTPError, RequestError, Timeout):
        raise ConnectionError
    answer = res.json()
    if "error" in answer:
        raise Exception(answer["errorMessage"])
    return answer


def restore(username, email):
    """
    :rtype: dict
    :type username: str
    :type email: str
    """
    if not username and not email:
        raise ValueError

    try:
        res = requests_wrap.post(RESTORE_URL, json={"username": username, "email": email})
    except (HTTPError, RequestError, Timeout):
        raise ConnectionError
    if res.status_code != 200:
        raise ConnectionError
    return res.json()
