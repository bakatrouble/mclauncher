import os
from requests import get
import tarfile


def dl_file(uri, destination, start_callback=None, chunk_callback=None, finish_callback=None):
    """
    :type uri: str
    :type destination: str
    :type start_callback: collections.abc.Callable
    :type chunk_callback: collections.abc.Callable
    :type finish_callback: collections.abc.Callable
    """
    if not uri.startswith("http:"):
        uri = "http:" + uri
    req = get(uri, stream=True, timeout=10)
    total = int(req.headers.get("content-length"))/1024/1024
    start_callback(total)
    done = 0

    file = open(os.path.join(destination, os.path.basename(uri)), "wb")
    for chunk in req.iter_content(chunk_size=1024*128):
        done += len(chunk)/1024/1024
        file.write(chunk)
        if chunk_callback:
            chunk_callback(done, total)
    file.close()
    if finish_callback:
        finish_callback()


def un_tar(path, destination, unlink=False, callback=None):
    """
    :type path: str
    :type destination: str
    :type unlink: bool
    :type callback: collections.abc.Callable
    """
    file = tarfile.open(path)

    try:
        file.extractall(destination)
    except EOFError:
        pass

    file.close()

    if unlink:
        os.unlink(path)
    if callback:
        callback()
