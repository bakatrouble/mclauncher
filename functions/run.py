from subprocess import Popen, PIPE
import subprocess
from os import path
from platform import system
from uuid import uuid4


def run_game(game_path, game_data, main_class, version, uuid=str(uuid4()), xmx=1024, max_perm_size=128, more=list()):
    """
    :type game_path: str
    :type game_data: str
    :type main_class: str
    :type version: str
    :type uuid: str
    :type xmx: int
    :type max_perm_size: int
    :type more: list
    """

    java_exe = "java" if system() != "Windows" else "java"

    # try:
    #     # startupinfo = None
    #     # if system() == "Windows":
    #     #     startupinfo = subprocess.STARTUPINFO()
    #     #     startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    #     p = Popen(["java", "-version"], stderr=PIPE)  # , startupinfo=startupinfo)
    # except FileNotFoundError as e:
    #     raise JavaNotFound("Java is either not installed or installed incorrectly. Please, use 64-bit version.\n"
    #                        "Do you want to proceed to the download page right now?")
    # p.wait()
    # if b"64-Bit" not in p.communicate()[1]:
    #     raise JavaNotFound("64-bit Java is needed, but 32-bit version is installed.\n"
    #                        "Do you want to proceed to the download page right now?")

    args = [
        java_exe,
        "-XX:+UseG1GC",
        "-XX:InitiatingHeapOccupancyPercent=60",
        "-XX:MaxGCPauseMillis=200",
        "-Xms" + str(xmx) + "m",
        "-Xmx" + str(xmx) + "m",
        "-XX:-UseAdaptiveSizePolicy",
        "-Xmn128M",
        "-Djava.library.path=" + path.join(game_path, "natives"),
        "-cp",
        path.join(game_path, "minecraft.jar"),
        main_class,
        "--version", version,
        "--gameDir", game_data,
        "--assetsDir", path.join(game_path, "assets"),
        "--assetIndex", "1.7.10",
        "--userProperties", "{}",
        "--accessToken", "123456",
        "--uuid", uuid,
        "-XX:MaxPermSize=" + str(max_perm_size) + "m",
    ] + more

    return Popen(args)


class JavaNotFound(Exception):
    def __init__(self, message):
        self.message = message

if __name__ == '__main__':
    run_game(game_path="game", game_data="gamedata", main_class="net.minecraft.client.main.Main",
             version="1.7.10", xmx=1024)
