import sys
from PySide.QtGui import *
from classes import MainWindow
from ui import Defaults
from ui.replace import replace_widgets

app = QApplication(sys.argv)
app.setStyle("cleanlooks")

i = QFontDatabase.addApplicationFont(":/fonts/Minecraft.ttf")
app.setFont(Defaults.font)

replace_widgets()

win = MainWindow()

if not Defaults.bright_bg:
    win.setStyleSheet("color: #fff;")

win.setStyleSheet("background-image: url(:/images/background.png);")

win.show()

win.login_page.check_saved()

sys.exit(app.exec_())
