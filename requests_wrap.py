import requests
from config import *


def get(url, params=None, **kwargs):
    kwargs["timeout"] = TIMEOUT
    return requests.get(url, params, **kwargs)


def post(url, data=None, json=None, **kwargs):
    kwargs["timeout"] = TIMEOUT
    return requests.post(url, data, json, **kwargs)
