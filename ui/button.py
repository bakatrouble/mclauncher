from PySide.QtCore import Qt
from PySide.QtGui import QPainter, QBrush, QFontMetrics, QAbstractButton, QColor
from ui import Defaults


class Button(QAbstractButton):
    color = Defaults.color
    text_width = 0
    un_hovered = False

    def __init__(self, parent=None, flat=False):
        super().__init__(parent)
        self.setCursor(Qt.PointingHandCursor)
        self.setUpdatesEnabled(True)

        self.flat = flat

    def paintEvent(self, e):
        painter = QPainter(self)
        self.draw_widget(painter)

    def draw_widget(self, painter):
        width = self.width()
        height = self.height()
        text = self.text()
        hovered = self.underMouse()
        pressed = self.isDown()

        if self.flat:
            painter.setBrush(QBrush(QColor("#ddd") if hovered and not self.un_hovered else Qt.white))
        else:
            saturation = 700 if hovered and not self.un_hovered else 500
            painter.setBrush(QBrush(self.color.get_color(saturation)))
        painter.setPen(Qt.NoPen)
        painter.drawRect(0, 0, width, height)
        painter.setPen(self.color.get_color() if self.flat else Qt.white)
        painter.drawText(
                (width-self.text_width) // 2,
                height - Defaults.padding + (1 if hovered and pressed else 0),
                text)

    def setText(self, text):
        super(Button, self).setText(text)
        fm = QFontMetrics(self.font(), self)
        self.text_width = fm.width(text)
        padding = Defaults.padding * 2
        self.setMinimumSize(self.text_width+padding, fm.ascent()+padding)

    def hitButton(self, *args, **kwargs):
        return True

    def mousePressEvent(self, e):
        if e.button() != Qt.LeftButton:
            self.setDown(False)
            e.setAccepted(True)
            return
        self.pressed.emit()
        self.setDown(True)
        self.update()

    def mouseReleaseEvent(self, e):
        if e.button() != Qt.LeftButton:
            self.un_hovered = False
            self.setDown(False)
            return
        self.released.emit()
        if not self.un_hovered:
            self.clicked.emit()
        self.setDown(False)
        self.update()

    def mouseMoveEvent(self, e):
        x = e.x()
        y = e.y()
        width = self.width()
        height = self.height()
        if x < 0 or y < 0 or x > width or y > height:
            self.setDown(False)
            self.un_hovered = True
            self.update()
        else:
            self.setDown(True)
            self.un_hovered = False
            self.update()










