from PySide.QtGui import QColor


class Color:
    _color = QColor()

    Blue = "#2196f3"

    tunes = {
        50: 152,
        100: 137,
        200: 126,
        300: 112,
        400: 106,
        500: 100,
        600: 94,
        700: 88,
        800: 82,
        900: 76
    }

    def __init__(self, hex_color):
        self._color.setNamedColor(hex_color)

    def get_color(self, saturation=500):
        return self._color.lighter(self.tunes[saturation])

