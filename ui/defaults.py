from PySide.QtGui import QFont

from ui import Color


class Defaults:
    color = Color(Color.Blue)
    padding = 10
    font = QFont("Minecraft 1.1")
    font.setStyleStrategy(QFont.NoAntialias)

    bright_bg = False
