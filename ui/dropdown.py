from PySide.QtCore import Qt, QPoint, QSize
from PySide.QtGui import QComboBox, QPainter, QFontMetrics, QListView, QStyledItemDelegate, \
    QStyle, QColor

from ui import Defaults, Color


class ListView(QListView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setStyleSheet("background-color: #fff;"
                           "border: none;"
                           "padding: 0;")
        self.setContentsMargins(0, 0, 0, 0)


class ItemDelegate(QStyledItemDelegate):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def sizeHint(self, option, index):
        fm = QFontMetrics(Defaults.font)
        width = 0
        for row in range(0, index.model().rowCount()):
            idx = index.model().index(row, 0)
            width = max(width, fm.width(index.model().data(idx)))

        height = fm.ascent()+Defaults.padding*2
        return QSize(width, height)

    def paint(self, painter, option, index):
        if option.state & QStyle.State_Selected:
            painter.setPen(Qt.NoPen)
            painter.setBrush(QColor("rgba(255, 255, 255, 50)"))  # Color(Color.Blue).get_color())
            painter.drawRect(option.rect)
            painter.setPen(Qt.white)
        else:
            painter.setPen(Qt.black if Defaults.bright_bg else Qt.white)
        painter.drawText(Defaults.padding, option.rect.top()+option.rect.height()-Defaults.padding,
                         index.model().data(index))


class DropDown(QComboBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fm = self.fontMetrics()
        self.setMinimumHeight(fm.ascent()+Defaults.padding*2)
        self.setView(ListView())
        self.setItemDelegate(ItemDelegate())
        if not Defaults.bright_bg:
            self.setStyleSheet("color: #fff;")      # color

    def paintEvent(self, e):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.TextAntialiasing)
        self.draw_widget(painter)

    def draw_widget(self, painter):
        width = self.width()
        height = self.height()

        fm = self.fontMetrics()
        triangleX = fm.width(self.currentText()) + Defaults.padding

        if not Defaults.bright_bg:
            painter.setPen(Qt.white)                # color
            painter.setBrush(Qt.white)              # color
        else:
            painter.setPen(Qt.black)                # color
            painter.setBrush(Qt.black)              # color
        painter.drawText(0, height-Defaults.padding, self.currentText())
        painter.drawPolygon([
            QPoint(triangleX+4, height//2-1),
            QPoint(triangleX, height//2-1),
            QPoint(triangleX+2, height//2+4),
        ])
