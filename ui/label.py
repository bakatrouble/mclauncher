from PySide.QtCore import QSize, Qt
from PySide.QtGui import QLabel, QPainter

from ui import Defaults


class Label(QLabel):
    def __init__(self, *args, **kwargs):
        super(Label, self).__init__(*args, **kwargs)
        self.setStyleSheet("color: #000;")
        if not Defaults.bright_bg:
            self.setStyleSheet("color: #fff;")
        self.setAttribute(Qt.WA_TranslucentBackground)

    def sizeHint(self, *args, **kwargs):
        fm = self.fontMetrics()
        return QSize(fm.width(self.text()), fm.ascent()+Defaults.padding*2)

    def paintEvent(self, e):
        painter = QPainter(self)
        # painter.setRenderHint(QPainter.TextAntialiasing)
        painter.drawText(0, self.height()-Defaults.padding, self.text())
