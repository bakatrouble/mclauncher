from PySide.QtGui import QLineEdit
from ui import Color, Defaults


class LineEdit(QLineEdit):
    color = Color(Color.Blue)
    base_style = ("background: rgba(0, 0, 0, 0);"
                  "border: none;"
                  "border-bottom: 2px solid {};"
                  "padding: 0 0 1px 0;"
                  "lineedit-password-character: 0042;")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setStyleSheet(
            (self.base_style + ("color: #fff;" if not Defaults.bright_bg else "")).format("#aaa")
        )
        print(self.inputMask())

    def focusInEvent(self, *args, **kwargs):
        super(LineEdit, self).focusInEvent(*args, **kwargs)
        self.setStyleSheet(
            (self.base_style + ("color: #fff;" if not Defaults.bright_bg else "")).format(self.color.get_color().name())
        )

    def focusOutEvent(self, *args, **kwargs):
        super().focusOutEvent(*args, **kwargs)
        self.setStyleSheet(
            (self.base_style + ("color: #fff;" if not Defaults.bright_bg else "")).format("#aaa")
        )
