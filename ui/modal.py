from PySide.QtCore import QTimeLine, Qt
from PySide.QtGui import QFrame, QLabel, QPushButton, QWidget, QPainter, QHBoxLayout, QPen, QSpacerItem, QSizePolicy

from ui import Button, Color
from views.ui_modal import Ui_Modal


class Modal(QFrame, Ui_Modal):
    def __init__(self, parent=None):
        super(Modal, self).__init__(parent)
        self.setupUi(self)
        self.setVisible(False)
        self.label = QLabel()
        self.pb = self.ProgressBar()

    def message_box(self, text):
        if self.isVisible():
            self.close_modal()
        self.title.setVisible(False)
        self.label = QLabel(text)
        self.label.setWordWrap(True)
        self.label.setStyleSheet("color: #000;")                             # color
        self.main_area.addWidget(self.label)
        ok_btn = Button(flat=True)
        ok_btn.setText("OK")
        ok_btn.clicked.connect(self.close_modal)
        self.buttons.addWidget(ok_btn)
        self.setVisible(True)

    def progress_bar(self, text, waiting_cursor=False):
        self.title.setVisible(False)
        lay = QHBoxLayout()
        self.pb.tl.start()
        self.label = QLabel()
        self.label.setText(text)
        self.label.setWordWrap(True)
        self.label.setStyleSheet("color: #000;")                             # color
        lay.addWidget(self.pb)
        lay.addWidget(self.label)
        lay.addItem(QSpacerItem(1, 1, QSizePolicy.Expanding, QSizePolicy.Minimum))
        w = QWidget()
        w.setLayout(lay)
        self.main_area.addWidget(w)

        if waiting_cursor:
            self.setCursor(Qt.WaitCursor)

        self.setVisible(True)

    def yes_no_dialog(self, text, yes_cb=None, no_cb=None):
        def yes():
            self.close_modal()
            if yes_cb:
                yes_cb()

        def no():
            self.close_modal()
            if no_cb:
                no_cb()

        if self.isVisible():
            self.close_modal()
        self.title.setVisible(False)
        self.label = QLabel(text)
        self.label.setWordWrap(True)
        self.label.setStyleSheet("color: #000;")                             # color
        self.main_area.addWidget(self.label)
        no_btn = Button(flat=True)
        no_btn.setText("No")
        no_btn.clicked.connect(no)
        self.buttons.addWidget(no_btn)
        yes_btn = Button(flat=True)
        yes_btn.setText("Yes")
        yes_btn.clicked.connect(yes)
        self.buttons.addWidget(yes_btn)
        self.setVisible(True)

    class ProgressBar(QWidget):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.setFixedSize(30, 30)
            self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
            self.tl = QTimeLine()
            self.tl.setDuration(1000)
            self.tl.setLoopCount(0)
            self.tl.setCurveShape(QTimeLine.LinearCurve)
            self.tl.setUpdateInterval(1000//60)
            self.tl.valueChanged.connect(self.repaint)

        def paintEvent(self, *args, **kwargs):
            painter = QPainter(self)
            painter.setRenderHint(QPainter.Antialiasing)
            pen = QPen(Color(Color.Blue).get_color())
            pen.setWidth(3)
            painter.setPen(pen)
            phase = self.tl.currentValue()
            painter.drawArc(2, 2, self.width()-4, self.height()-4, -phase*360*16, 270*16)

    def close_modal(self):
        self.setVisible(False)
        for i in reversed(range(self.buttons.count())):
            self.buttons.itemAt(i).widget().setParent(None)
        for i in reversed(range(self.main_area.count())):
            self.main_area.itemAt(i).widget().setParent(None)
        self.label = QLabel()
        self.pb = self.ProgressBar()
        self.setCursor(Qt.ArrowCursor)
