from ui import Button, LineEdit, Label, DropDown
from PySide import QtGui


def replace_widgets():
    QtGui.QLabel = Label
    QtGui.QPushButton = Button
    QtGui.QLineEdit = LineEdit
    QtGui.QComboBox = DropDown
