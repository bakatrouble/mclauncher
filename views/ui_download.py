# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'download.ui'
#
# Created: Mon Feb  1 23:00:00 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_DownloadPage(object):
    def setupUi(self, DownloadPage):
        DownloadPage.setObjectName("DownloadPage")
        DownloadPage.resize(300, 400)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(DownloadPage.sizePolicy().hasHeightForWidth())
        DownloadPage.setSizePolicy(sizePolicy)
        self.gridLayout_2 = QtGui.QGridLayout(DownloadPage)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.widget = QtGui.QWidget(DownloadPage)
        self.widget.setObjectName("widget")
        self.layoutWidget = QtGui.QWidget(self.widget)
        self.layoutWidget.setGeometry(QtCore.QRect(9, 9, 281, 381))
        self.layoutWidget.setObjectName("layoutWidget")
        self.grid_layout = QtGui.QGridLayout(self.layoutWidget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        self.grid_layout.setVerticalSpacing(0)
        self.grid_layout.setObjectName("grid_layout")
        self.download_label = QtGui.QLabel(self.layoutWidget)
        self.download_label.setObjectName("download_label")
        self.grid_layout.addWidget(self.download_label, 3, 0, 1, 1)
        self.download_progress = QtGui.QProgressBar(self.layoutWidget)
        self.download_progress.setMaximum(0)
        self.download_progress.setProperty("value", -1)
        self.download_progress.setObjectName("download_progress")
        self.grid_layout.addWidget(self.download_progress, 5, 0, 1, 1)
        self.speed_label = QtGui.QLabel(self.layoutWidget)
        self.speed_label.setObjectName("speed_label")
        self.grid_layout.addWidget(self.speed_label, 4, 0, 1, 1)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.grid_layout.addItem(spacerItem, 6, 0, 1, 1)
        self.gridLayout_2.addWidget(self.widget, 0, 0, 1, 1)

        self.retranslateUi(DownloadPage)
        QtCore.QMetaObject.connectSlotsByName(DownloadPage)

    def retranslateUi(self, DownloadPage):
        DownloadPage.setWindowTitle(QtGui.QApplication.translate("DownloadPage", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.download_label.setText(QtGui.QApplication.translate("DownloadPage", "Downloading...", None, QtGui.QApplication.UnicodeUTF8))
        self.download_progress.setFormat(QtGui.QApplication.translate("DownloadPage", "%v/%m", None, QtGui.QApplication.UnicodeUTF8))
        self.speed_label.setText(QtGui.QApplication.translate("DownloadPage", "0 bps", None, QtGui.QApplication.UnicodeUTF8))

