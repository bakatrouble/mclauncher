# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created: Mon Feb 22 23:19:42 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_LoginPage(object):
    def setupUi(self, LoginPage):
        LoginPage.setObjectName("LoginPage")
        LoginPage.resize(300, 400)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(LoginPage.sizePolicy().hasHeightForWidth())
        LoginPage.setSizePolicy(sizePolicy)
        self.gridLayout_2 = QtGui.QGridLayout(LoginPage)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.widget = QtGui.QWidget(LoginPage)
        self.widget.setObjectName("widget")
        self.gridLayoutWidget = QtGui.QWidget(self.widget)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(9, 9, 281, 381))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.grid_layout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        self.grid_layout.setObjectName("grid_layout")
        self.to_register = QtGui.QPushButton(self.gridLayoutWidget)
        self.to_register.setFocusPolicy(QtCore.Qt.NoFocus)
        self.to_register.setObjectName("to_register")
        self.grid_layout.addWidget(self.to_register, 7, 0, 1, 1)
        self.to_restore = QtGui.QPushButton(self.gridLayoutWidget)
        self.to_restore.setFocusPolicy(QtCore.Qt.NoFocus)
        self.to_restore.setObjectName("to_restore")
        self.grid_layout.addWidget(self.to_restore, 7, 1, 1, 1)
        self.do_login_btn = QtGui.QPushButton(self.gridLayoutWidget)
        self.do_login_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.do_login_btn.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.do_login_btn.setObjectName("do_login_btn")
        self.grid_layout.addWidget(self.do_login_btn, 5, 0, 1, 2)
        self.password_input = QtGui.QLineEdit(self.gridLayoutWidget)
        self.password_input.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.password_input.setEchoMode(QtGui.QLineEdit.Password)
        self.password_input.setObjectName("password_input")
        self.grid_layout.addWidget(self.password_input, 3, 0, 1, 2)
        self.password_label = QtGui.QLabel(self.gridLayoutWidget)
        self.password_label.setEnabled(True)
        self.password_label.setObjectName("password_label")
        self.grid_layout.addWidget(self.password_label, 2, 0, 1, 2)
        self.username_label = QtGui.QLabel(self.gridLayoutWidget)
        self.username_label.setObjectName("username_label")
        self.grid_layout.addWidget(self.username_label, 0, 0, 1, 2)
        self.username_input = QtGui.QLineEdit(self.gridLayoutWidget)
        self.username_input.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.username_input.setObjectName("username_input")
        self.grid_layout.addWidget(self.username_input, 1, 0, 1, 2)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.grid_layout.addItem(spacerItem, 8, 0, 1, 2)
        self.remember_me = QtGui.QCheckBox(self.gridLayoutWidget)
        self.remember_me.setChecked(True)
        self.remember_me.setObjectName("remember_me")
        self.grid_layout.addWidget(self.remember_me, 4, 0, 1, 2)
        self.grid_layout.setColumnStretch(0, 2)
        self.grid_layout.setColumnStretch(1, 3)
        self.gridLayout_2.addWidget(self.widget, 0, 0, 1, 1)

        self.retranslateUi(LoginPage)
        QtCore.QMetaObject.connectSlotsByName(LoginPage)
        LoginPage.setTabOrder(self.username_input, self.password_input)

    def retranslateUi(self, LoginPage):
        LoginPage.setWindowTitle(QtGui.QApplication.translate("LoginPage", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.to_register.setText(QtGui.QApplication.translate("LoginPage", "Register", None, QtGui.QApplication.UnicodeUTF8))
        self.to_restore.setText(QtGui.QApplication.translate("LoginPage", "Recovery", None, QtGui.QApplication.UnicodeUTF8))
        self.do_login_btn.setText(QtGui.QApplication.translate("LoginPage", "Login", None, QtGui.QApplication.UnicodeUTF8))
        self.password_label.setText(QtGui.QApplication.translate("LoginPage", "Password", None, QtGui.QApplication.UnicodeUTF8))
        self.username_label.setText(QtGui.QApplication.translate("LoginPage", "Username", None, QtGui.QApplication.UnicodeUTF8))
        self.remember_me.setText(QtGui.QApplication.translate("LoginPage", "Remember me", None, QtGui.QApplication.UnicodeUTF8))

