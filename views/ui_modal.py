# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'modal.ui'
#
# Created: Sat Feb  6 14:25:44 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Modal(object):
    def setupUi(self, Modal):
        Modal.setObjectName("Modal")
        Modal.resize(300, 400)
        Modal.setStyleSheet("background: rgba(0, 0, 0, 100);")
        self.verticalLayout = QtGui.QVBoxLayout(Modal)
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem = QtGui.QSpacerItem(20, 151, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.frame = QtGui.QFrame(Modal)
        self.frame.setMinimumSize(QtCore.QSize(0, 10))
        self.frame.setStyleSheet("background:#eee;")
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.frame)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.title = QtGui.QLabel(self.frame)
        self.title.setStyleSheet("font-size:20px; font-weight: bold")
        self.title.setObjectName("title")
        self.verticalLayout_2.addWidget(self.title)
        self.main_area = QtGui.QGridLayout()
        self.main_area.setObjectName("main_area")
        self.verticalLayout_2.addLayout(self.main_area)
        self.horiz = QtGui.QHBoxLayout()
        self.horiz.setObjectName("horiz")
        spacerItem1 = QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horiz.addItem(spacerItem1)
        self.buttons = QtGui.QHBoxLayout()
        self.buttons.setObjectName("buttons")
        self.horiz.addLayout(self.buttons)
        self.verticalLayout_2.addLayout(self.horiz)
        self.verticalLayout.addWidget(self.frame)
        spacerItem2 = QtGui.QSpacerItem(20, 151, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)

        self.retranslateUi(Modal)
        QtCore.QMetaObject.connectSlotsByName(Modal)

    def retranslateUi(self, Modal):
        Modal.setWindowTitle(QtGui.QApplication.translate("Modal", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.title.setText(QtGui.QApplication.translate("Modal", "Title", None, QtGui.QApplication.UnicodeUTF8))

