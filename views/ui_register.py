# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'register.ui'
#
# Created: Mon Feb  1 22:59:53 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_RegisterPage(object):
    def setupUi(self, RegisterPage):
        RegisterPage.setObjectName("RegisterPage")
        RegisterPage.resize(300, 400)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(RegisterPage.sizePolicy().hasHeightForWidth())
        RegisterPage.setSizePolicy(sizePolicy)
        self.gridLayout = QtGui.QGridLayout(RegisterPage)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.widget = QtGui.QWidget(RegisterPage)
        self.widget.setObjectName("widget")
        self.gridLayoutWidget_2 = QtGui.QWidget(self.widget)
        self.gridLayoutWidget_2.setGeometry(QtCore.QRect(9, 9, 281, 381))
        self.gridLayoutWidget_2.setObjectName("gridLayoutWidget_2")
        self.grid_layout = QtGui.QGridLayout(self.gridLayoutWidget_2)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        self.grid_layout.setObjectName("grid_layout")
        self.email_label = QtGui.QLabel(self.gridLayoutWidget_2)
        self.email_label.setObjectName("email_label")
        self.grid_layout.addWidget(self.email_label, 6, 0, 1, 2)
        self.password_input = QtGui.QLineEdit(self.gridLayoutWidget_2)
        self.password_input.setEchoMode(QtGui.QLineEdit.Password)
        self.password_input.setObjectName("password_input")
        self.grid_layout.addWidget(self.password_input, 3, 0, 1, 2)
        self.email_input = QtGui.QLineEdit(self.gridLayoutWidget_2)
        self.email_input.setInputMethodHints(QtCore.Qt.ImhEmailCharactersOnly)
        self.email_input.setObjectName("email_input")
        self.grid_layout.addWidget(self.email_input, 7, 0, 1, 2)
        self.username_input = QtGui.QLineEdit(self.gridLayoutWidget_2)
        self.username_input.setObjectName("username_input")
        self.grid_layout.addWidget(self.username_input, 1, 0, 1, 2)
        self.do_register_btn = QtGui.QPushButton(self.gridLayoutWidget_2)
        self.do_register_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.do_register_btn.setObjectName("do_register_btn")
        self.grid_layout.addWidget(self.do_register_btn, 8, 0, 1, 1)
        self.password_label = QtGui.QLabel(self.gridLayoutWidget_2)
        self.password_label.setObjectName("password_label")
        self.grid_layout.addWidget(self.password_label, 2, 0, 1, 2)
        self.to_login = QtGui.QPushButton(self.gridLayoutWidget_2)
        self.to_login.setFocusPolicy(QtCore.Qt.NoFocus)
        self.to_login.setObjectName("to_login")
        self.grid_layout.addWidget(self.to_login, 8, 1, 1, 1)
        self.username_label = QtGui.QLabel(self.gridLayoutWidget_2)
        self.username_label.setObjectName("username_label")
        self.grid_layout.addWidget(self.username_label, 0, 0, 1, 2)
        self.password_input2 = QtGui.QLineEdit(self.gridLayoutWidget_2)
        self.password_input2.setEchoMode(QtGui.QLineEdit.Password)
        self.password_input2.setObjectName("password_input2")
        self.grid_layout.addWidget(self.password_input2, 5, 0, 1, 2)
        self.password_label2 = QtGui.QLabel(self.gridLayoutWidget_2)
        self.password_label2.setObjectName("password_label2")
        self.grid_layout.addWidget(self.password_label2, 4, 0, 1, 2)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.grid_layout.addItem(spacerItem, 9, 0, 1, 2)
        self.gridLayout.addWidget(self.widget, 0, 0, 1, 1)

        self.retranslateUi(RegisterPage)
        QtCore.QMetaObject.connectSlotsByName(RegisterPage)
        RegisterPage.setTabOrder(self.username_input, self.password_input)
        RegisterPage.setTabOrder(self.password_input, self.password_input2)
        RegisterPage.setTabOrder(self.password_input2, self.email_input)
        RegisterPage.setTabOrder(self.email_input, self.to_login)
        RegisterPage.setTabOrder(self.to_login, self.do_register_btn)

    def retranslateUi(self, RegisterPage):
        RegisterPage.setWindowTitle(QtGui.QApplication.translate("RegisterPage", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.email_label.setText(QtGui.QApplication.translate("RegisterPage", "E-mail", None, QtGui.QApplication.UnicodeUTF8))
        self.do_register_btn.setText(QtGui.QApplication.translate("RegisterPage", "Register", None, QtGui.QApplication.UnicodeUTF8))
        self.password_label.setText(QtGui.QApplication.translate("RegisterPage", "Password", None, QtGui.QApplication.UnicodeUTF8))
        self.to_login.setText(QtGui.QApplication.translate("RegisterPage", "Back", None, QtGui.QApplication.UnicodeUTF8))
        self.username_label.setText(QtGui.QApplication.translate("RegisterPage", "Username", None, QtGui.QApplication.UnicodeUTF8))
        self.password_label2.setText(QtGui.QApplication.translate("RegisterPage", "Repeat password", None, QtGui.QApplication.UnicodeUTF8))

