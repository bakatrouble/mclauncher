# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'restore.ui'
#
# Created: Mon Feb  1 22:59:57 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_RestorePage(object):
    def setupUi(self, RestorePage):
        RestorePage.setObjectName("RestorePage")
        RestorePage.resize(300, 400)
        self.gridLayout_2 = QtGui.QGridLayout(RestorePage)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.widget = QtGui.QWidget(RestorePage)
        self.widget.setObjectName("widget")
        self.gridLayoutWidget = QtGui.QWidget(self.widget)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(9, 9, 281, 381))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.grid_layout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        self.grid_layout.setObjectName("grid_layout")
        self.username_input = QtGui.QLineEdit(self.gridLayoutWidget)
        self.username_input.setObjectName("username_input")
        self.grid_layout.addWidget(self.username_input, 1, 0, 1, 2)
        self.username_label = QtGui.QLabel(self.gridLayoutWidget)
        self.username_label.setFocusPolicy(QtCore.Qt.NoFocus)
        self.username_label.setObjectName("username_label")
        self.grid_layout.addWidget(self.username_label, 0, 0, 1, 2)
        self.do_restore_btn = QtGui.QPushButton(self.gridLayoutWidget)
        self.do_restore_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.do_restore_btn.setObjectName("do_restore_btn")
        self.grid_layout.addWidget(self.do_restore_btn, 5, 0, 1, 1)
        self.to_login = QtGui.QPushButton(self.gridLayoutWidget)
        self.to_login.setFocusPolicy(QtCore.Qt.NoFocus)
        self.to_login.setObjectName("to_login")
        self.grid_layout.addWidget(self.to_login, 5, 1, 1, 1)
        self.email_label = QtGui.QLabel(self.gridLayoutWidget)
        self.email_label.setFocusPolicy(QtCore.Qt.NoFocus)
        self.email_label.setObjectName("email_label")
        self.grid_layout.addWidget(self.email_label, 3, 0, 1, 2)
        self.email_input = QtGui.QLineEdit(self.gridLayoutWidget)
        self.email_input.setInputMethodHints(QtCore.Qt.ImhEmailCharactersOnly)
        self.email_input.setEchoMode(QtGui.QLineEdit.Normal)
        self.email_input.setObjectName("email_input")
        self.grid_layout.addWidget(self.email_input, 4, 0, 1, 2)
        self.or_label = QtGui.QLabel(self.gridLayoutWidget)
        self.or_label.setFocusPolicy(QtCore.Qt.NoFocus)
        self.or_label.setObjectName("or_label")
        self.grid_layout.addWidget(self.or_label, 2, 0, 1, 2)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.grid_layout.addItem(spacerItem, 6, 0, 1, 2)
        self.gridLayout_2.addWidget(self.widget, 0, 0, 1, 1)

        self.retranslateUi(RestorePage)
        QtCore.QMetaObject.connectSlotsByName(RestorePage)

    def retranslateUi(self, RestorePage):
        RestorePage.setWindowTitle(QtGui.QApplication.translate("RestorePage", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.username_label.setText(QtGui.QApplication.translate("RestorePage", "Username", None, QtGui.QApplication.UnicodeUTF8))
        self.do_restore_btn.setText(QtGui.QApplication.translate("RestorePage", "Restore", None, QtGui.QApplication.UnicodeUTF8))
        self.to_login.setText(QtGui.QApplication.translate("RestorePage", "Back", None, QtGui.QApplication.UnicodeUTF8))
        self.email_label.setText(QtGui.QApplication.translate("RestorePage", "E-mail", None, QtGui.QApplication.UnicodeUTF8))
        self.or_label.setText(QtGui.QApplication.translate("RestorePage", "or", None, QtGui.QApplication.UnicodeUTF8))

